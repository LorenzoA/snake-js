let canvas = document.getElementById('zone');//la zone de jeu
let context = canvas.getContext('2d'); //contexte de dessin, ici 2d
let loose = document.getElementById('loose');
let largeurSerpent = 25;
let hauteurSerpent = 25;
//position serpent aléatoire
let xPos = Math.trunc(Math.random() * canvas.width / largeurSerpent) * largeurSerpent;
let yPos = Math.trunc(Math.random() * canvas.height / hauteurSerpent) * hauteurSerpent;
console.log();
console.log(`position du serpent  ${xPos} :  ${yPos}`);
let axeX = 0;
let axeY = 0;
// la queue
let queue = [];
let tailleInitialeQueue = 5;

let tailleQueue = 5;
let increaseQueue = 1;
let tailleQueueMax = 150;
//compteur de cycle
let countCycles = 0;
let incrementCycles = 10;
let historiqueKey = 0;
//pomme
let pommeX = Math.trunc(Math.random() * canvas.width / largeurSerpent) * largeurSerpent;
let pommeY = Math.trunc(Math.random() * canvas.height / hauteurSerpent) * hauteurSerpent;
let pommeRadius = 5;
//score
let score = 0;
let scoreJoueur = document.getElementById('score');


window.onload = function () {

    document.addEventListener("keydown", move);
};


let refresh = setInterval(main, 150);

function main() {

    context.clearRect(0, 0, canvas.width, canvas.height); // ligne pour effacer le serpent au fur et a mesure


    if (xPos < 600 && xPos >= 0 && yPos < 600 && yPos >= 0) {// condition de de mouvement selon la position dans le canvas

        xPos += axeX * largeurSerpent;
        yPos += axeY * hauteurSerpent;
    } else {
        xPos = xPos;
        yPos = yPos;
        perdu();
    }
    queue.push({ xPos: xPos, yPos: yPos });
    context.fillStyle = "#f1c40f";
    console.log(`voivi la queue: ${queue}`);
    while (queue.length > tailleQueue) {

        queue.shift();
    }
    for (let i = 0; i < queue.length; i++) {
        //on dessine les carrés de la queue
        context.fillRect(queue[i].xPos, queue[i].yPos, largeurSerpent - 1, hauteurSerpent - 1);
    }
    //dessin de la pomme
    context.beginPath();
    context.arc(pommeX, pommeY, pommeRadius, 0, Math.PI * 2);
    context.fillStyle = "#e74c3c";
    context.fill();
    context.closePath();
    if (xPos == pommeX && yPos == pommeY) { // collison entre le snake et la pommeY

        score += 5 + 2 * ((tailleQueue - tailleInitialeQueue) / increaseQueue);
        console.log(score);

        if ((tailleQueue <= tailleQueueMax)) {
            tailleQueue += increaseQueue;
        }
        pommeX = Math.trunc(Math.random() * canvas.width / largeurSerpent) * largeurSerpent;
        pommeY = Math.trunc(Math.random() * canvas.height / hauteurSerpent) * hauteurSerpent;
    }
    scoring();
}

console.log(queue);


function scoring() {
    scoreJoueur.innerText = score;
}

function perdu() {
    clearInterval(refresh);
    loose.innerText = "Vous avez perdu !"
};

function move(event) {

    switch (event.keyCode) {
        case 37:
            // touche gauche
            if (historiqueKey == 39) { break; }
            axeX = -1;
            axeY = 0;
            historiqueKey = event.keyCode;
            break;
        case 38:
            if (historiqueKey == 40) { break; }
            axeX = 0;
            axeY = -1;
            historiqueKey = event.keyCode;
            // touche haut
            break;
        case 39:
            if (historiqueKey == 37) { break; }
            axeX = 1;
            axeY = 0;
            historiqueKey = event.keyCode;
            // touche droite
            break;
        case 40:
            if (historiqueKey == 38) { break; }
            axeX = 0;
            axeY = 1;
            historiqueKey = event.keyCode;
            // touche bas
            break;

        case 32:
            axeX = 0;
            axeY = 0;
            // touche espace

            break;

    }

}

